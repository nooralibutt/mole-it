package com.nab.moleit;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

import com.nab.tapthemole.Constant;

import android.app.Activity;
import android.os.Bundle;

public class Application extends Activity {

	protected CCGLSurfaceView _glSurfaceView;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	 
	    Constant.hideActionBar(Application.this);
	    
	    _glSurfaceView = new CCGLSurfaceView(this);
	 
	    setContentView(_glSurfaceView);
	}
	
	@Override
	public void onStart()
	{
	    super.onStart();
	 
	    CCDirector.sharedDirector().attachInView(_glSurfaceView);
	    CCDirector.sharedDirector().setDeviceOrientation(CCDirector.kCCDeviceOrientationLandscapeLeft);
	    CCDirector.sharedDirector().setDisplayFPS(false);
	    
	    CCDirector.sharedDirector().setAnimationInterval(1.0f / 60.0f);
	    
	    CCScene scene = Game.scene();
	    
	    CCDirector.sharedDirector().runWithScene(scene);
	}
	
	@Override
	public void onPause()
	{
	    super.onPause();
	 
	    CCDirector.sharedDirector().onPause();
	}
	 
	@Override
	public void onResume()
	{
	    super.onResume();
	 
	    CCDirector.sharedDirector().onResume();
	}
	 
	@Override
	public void onStop()
	{
	    super.onStop();
	 
	    CCDirector.sharedDirector().end();
	}

	@Override
	public void onBackPressed() {
		
		Pause p = (Pause)tags.getLayerWithTag(tags.TAG_PAUSE);
		p.pause(null);
	}
}
