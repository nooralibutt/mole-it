package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;

import com.nab.tapthemole.levelselector.MenuItemActivity;

import android.app.Activity;

public class InstructionLayer extends CCLayer {

	Popup popup;
	CCMenu menu;
	
	public InstructionLayer() {
		
		CCSprite pauseBSprite = CCSprite.sprite(CCSpriteFrameCache.spriteFrameByName("pause_button.png"));
		CCMenuItemSprite pauseButton = CCMenuItemSprite.item(pauseBSprite, pauseBSprite, this, "pause");
		pauseButton.setScaleX(tags.rX);
		pauseButton.setScaleY(tags.rY );
		menu = CCMenu.menu(pauseButton);
		menu.setPosition(pauseButton.getContentSize().width, pauseButton.getContentSize().height);
		addChild(menu, 1);
		
		popup = Popup.getPopupWithTitle("-- Insructions --");
		
		//getting level instruction
		Activity activity = CCDirector.sharedDirector().getActivity();

		int level_no = activity.getIntent().getExtras().getInt(MenuItemActivity.EXTRA_LEVEL_NO);
		
		String instruction = activity.getResources().getString(activity.getResources().
				getIdentifier("instruction_" + level_no, "string", activity.getPackageName()));
		
		//setting level instruction
		CCLabel descLabel = CCLabel.makeLabel(instruction
				, "TOONISH.ttf", 14);
		descLabel.setPosition(popup.getContentSize().width/2, 
				popup.getContentSize().height/2f + descLabel.getContentSize().height * 2f);
		popup.addChild(descLabel, 2);
		
		addChild(popup, 1);
	}

	public void pause(Object sender)
	{
		popup.show(true);
		popup.addButtonWithText("Resume", this, "resume");
		popup.addButtonWithText("Main", this, "mainMenu");
		menu.setVisible(false);
		
		Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
		g.setIsTouchEnabled(false);
		
		Pause p = (Pause)tags.getLayerWithTag(tags.TAG_PAUSE);
		p.menu.setVisible(false);
	}
	public void resume(Object sender)
	{
		popup.show(false);
		Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
		g.setIsTouchEnabled(true);
		menu.setVisible(true);
		popup.menu.removeAllChildren(true);
		
		Pause p = (Pause)tags.getLayerWithTag(tags.TAG_PAUSE);
		p.menu.setVisible(true);
	}
	public void mainMenu(Object sender)
	{
		CCDirector.sharedDirector().getActivity().finish();
	}
}
