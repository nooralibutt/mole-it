package com.nab.moleit;

import com.nab.tapthemole.Constant;
import com.nab.tapthemole.SoundDude;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;

public class StoreActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Constant.hideActionBar(StoreActivity.this);
		
		setContentView(R.layout.store_moreapp_activity);
		
		findViewById(R.id.btn_home_store).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				SoundDude.btnSound(getApplicationContext(), R.raw.home_btn_snd);
				Handler hld = new Handler();
				hld.postDelayed(new Runnable() {
					@Override
					public void run() {
						finish();
					}
				}, 300);
				
			}
		});
	}
	
	public void OnIconClick(View v)
	{
		String packageName = (String) v.getTag();
		if(packageName.equalsIgnoreCase("")) return;
		
		if(packageName.startsWith("http"))
		{
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(packageName)));
			return;
		}
		
		try 
		{		
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+packageName)));
		} 
		catch (android.content.ActivityNotFoundException anfe) 
		{
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packageName)));
		}
	}
}
