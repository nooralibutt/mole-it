package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class tags {
	public static int TAG_GAME_SCENE;
	public static int TAG_GAME_LAYER = 1;
	public static int TAG_HUD = 2;
	public static int TAG_PAUSE = 3;
	public static int TAG_GAMEOVER = 4;
	public static int TAG_REPEAT_ANIM = 5;
	public static int TAG_BACK_BUTTON = 6;
	public static int TAG_PLAY_BUTTON = 7;
	public static int TAG_INSTRUCTION_LAYER = 8;
	public static float rX;
	public static float rY;
	public static int molesUpCount;
	
	public static CCLayer getLayerWithTag(int tag)
	{
		CCLayer layer = null;
		CCScene scene = CCDirector.sharedDirector().getRunningScene();
		if(scene.getTag() == tags.TAG_GAME_SCENE)
		{
			layer = (CCLayer) scene.getChild(tag);
		}
		return layer;
	}
	public static void saveHighScore(int score)
	{
		SharedPreferences sp = CCDirector.sharedDirector().getActivity().getSharedPreferences("com.nab.moleit", 0);
		Editor e = sp.edit();
		e.putInt("highScore", score);
		e.commit();	
	}
	public static int getHighScore()
	{
		SharedPreferences sp = CCDirector.sharedDirector().getActivity().getSharedPreferences("com.nab.moleit", 0);
		return sp.getInt("highScore", 0);
	}
}
