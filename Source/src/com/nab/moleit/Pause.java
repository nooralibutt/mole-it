package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.types.CGSize;

public class Pause extends CCLayer {
	Popup popup;
	CCMenu menu;
	public Pause()
	{
		CGSize winSize = CCDirector.sharedDirector().winSize();
		CCSprite pauseBSprite = CCSprite.sprite(CCSpriteFrameCache.spriteFrameByName("pause_button.png"));
		CCMenuItemSprite pauseButton = CCMenuItemSprite.item(pauseBSprite, pauseBSprite, this, "pause");
		pauseButton.setScaleX(tags.rX);
		pauseButton.setScaleY(tags.rY );
		menu = CCMenu.menu(pauseButton);
		menu.setPosition(winSize.width - pauseButton.getContentSize().width, pauseButton.getContentSize().height);
		addChild(menu, 1);
		
		popup = Popup.getPopupWithTitle("--Pause--");
		addChild(popup, 1);
	}
	
	public void pause(Object sender)
	{
		popup.show(true);
		popup.addButtonWithText("Resume", this, "resume");
		popup.addButtonWithText("Main", this, "mainMenu");
		menu.setVisible(false);
		Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
		g.setIsTouchEnabled(false);
		
		InstructionLayer p = (InstructionLayer)tags.getLayerWithTag(tags.TAG_INSTRUCTION_LAYER);
		p.menu.setVisible(false);
	}
	public void resume(Object sender)
	{
		popup.show(false);
		Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
		g.setIsTouchEnabled(true);
		menu.setVisible(true);
		popup.menu.removeAllChildren(true);
		
		InstructionLayer p = (InstructionLayer)tags.getLayerWithTag(tags.TAG_INSTRUCTION_LAYER);
		p.menu.setVisible(true);
	}
	public void mainMenu(Object sender)
	{
		CCDirector.sharedDirector().getActivity().finish();
	}
}
