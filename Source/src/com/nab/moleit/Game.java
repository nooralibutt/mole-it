package com.nab.moleit;

import java.util.ArrayList;
import java.util.Random;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;

import android.content.Context;
import android.view.MotionEvent;

public class Game extends CCLayer {
	
	ArrayList<Mole> moles;
	int molesAtATime;
	float TimeBetweenMoles;
	float increaseMolesAtTime;
	float timeElapsed;
	float increaseElapsed;
	float totalTime;
	
	HUD hudLayer;
	
	static CCScene scene() {
		CCScene scene = CCScene.node();
		tags.TAG_GAME_SCENE = 0;
		scene.setTag(tags.TAG_GAME_SCENE);
		
		Game g = new Game();
		scene.addChild(g, 0, tags.TAG_GAME_LAYER);
		
		HUD h = new HUD();
		scene.addChild(h, 1, tags.TAG_HUD);
		
		InstructionLayer instructionLayer = new InstructionLayer();
		scene.addChild(instructionLayer, 1, tags.TAG_INSTRUCTION_LAYER);
		
		Pause p = new Pause();
		scene.addChild(p,1, tags.TAG_PAUSE);
		
		GameOver go = new GameOver();
		scene.addChild(go, 1, tags.TAG_GAMEOVER);
		
		return scene;
	}

	public Game()
	{
		moles = new ArrayList<Mole>();	
		molesAtATime = 3;
		TimeBetweenMoles = 0.5f;
		increaseMolesAtTime = 10.0f;
		
		CGSize WinSize = CCDirector.sharedDirector().winSize();
		
		// handling sound - preloading
		Context context = CCDirector.sharedDirector().getActivity();
		SoundEngine.sharedEngine().preloadSound(context, R.raw.moles_bg);
		SoundEngine.sharedEngine().preloadEffect(context, R.raw.splat);
		SoundEngine.sharedEngine().preloadEffect(context, R.raw.moles_miss);

		// registering texture packer or sprite sheet
		// String file = (WinSize.width > 800) ? "moles-hd.plist" :
		// "moles.plist";
		CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFrames(
				"moles.plist");

		// setting background
		//String file = (WinSize.width > 800) ? "moles_bg-hd.png": "moles_bg.png";
		CCSprite bg = CCSprite.sprite("moles_bg.png");
		bg.setAnchorPoint(0, 0);
		addChild(bg, -1);
		float rX = WinSize.width / bg.getContentSize().width;
		float rY = WinSize.height / bg.getContentSize().height;

		bg.setScaleX(rX);
		bg.setScaleY(rY);
		
		tags.rX = rX * 0.95f;
		tags.rY = rY * 0.95f;
	}
	@Override
	public void onEnter() {
		super.onEnter();
		setIsTouchEnabled(true);
		initializeGame();
	}

	private void initializeGame() {
		
		hudLayer = (HUD)tags.getLayerWithTag(tags.TAG_HUD);

		tags.molesUpCount = 0;
		
		hudLayer.setTargetMoles(60);
		hudLayer.setTargetTime(60);
		
		Context context = CCDirector.sharedDirector().getActivity();
		SoundEngine.sharedEngine().playSound(context, R.raw.moles_bg, true);
		CGSize WinSize = CCDirector.sharedDirector().winSize();
		// creating dummy mole to populate hPad - horizontal padding 
		// and vPad - vertical Padding
		Mole mol = new Mole();
		float hPad = WinSize.width/2f - (mol.getContentSize().width * tags.rX * 3.5f);
		float vPad = WinSize.height/2f - (mol.getContentSize().height * tags.rY * 2.3f);
		mol.cleanup();

		for(int i = 1; i <= 4; i++){
			for(int j = 1; j <= 6; j++){
				Mole mole = new Mole();
				mole.setScaleX(tags.rX);
				mole.setScaleY(tags.rY);
				mole.setPosition(CGPoint.ccp(j * mole.getContentSize().width * tags.rX + hPad - (mol.getContentSize().width/2),
						i * mole.getContentSize().height * tags.rY + vPad - (mol.getContentSize().height/4)));
				mole.setAnchorPoint(0,0);
				moles.add(mole);
				addChild(mole,1);
			}
		}
		
		this.schedule("tick");
		
		InstructionLayer p = (InstructionLayer)tags.getLayerWithTag(tags.TAG_INSTRUCTION_LAYER);
		p.pause(null);
	}

	@Override
	public void onExit() {
		super.onExit();
		SoundEngine.sharedEngine().stopSound();
		SoundEngine.sharedEngine().reset(R.raw.moles_bg);
		moles.clear();
	}
	public void tick(float dt)
	{
		hudLayer.update_time(dt);
		
		timeElapsed += dt;	//represents time between moles
		increaseElapsed += dt;	//represents time when game gets harder and harder
		totalTime += dt;
		if(timeElapsed <= TimeBetweenMoles)
			return;
		timeElapsed = 0;
		if(tags.molesUpCount >= molesAtATime )
			return;
		/*int count = 0;
		for(int i = 0; i < moles.size(); i++)
		{
			if(moles.get(i).isUp)
			{
				count++;
				//mole capacity on screen has reached 
				if(count >= molesAtATime)
					return;
			}
		}*/
		//Log.d("NAB", "Time: "+totalTime+" moles at once: "+molesAtATime);
		tags.molesUpCount++;
		show();
		if(increaseElapsed > increaseMolesAtTime)
		{
			int maxMolesAtTime = 8;
			if(molesAtATime > maxMolesAtTime)
				return;
			molesAtATime++;
			increaseMolesAtTime *= 1.5f;
			float minTimeBetweenMoles = 0.1f;
			TimeBetweenMoles -= (TimeBetweenMoles > minTimeBetweenMoles)? 0.05f : 0f;
			increaseElapsed = 0;
		}
	}
	public void show()
	{
		Random r = new Random();
		int num = r.nextInt(24);
		while(moles.get(num).isUp)
		{
			num = r.nextInt(24);
		}
		moles.get(num).start(r.nextInt(Mole.TOTAL_MOLES));
	}
	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		//super.ccTouchesBegan(event);
		CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
		int i;
		for(i = 0; i < moles.size(); i++)
		{
			if( moles.get(i).isUp && CGRect.containsPoint(moles.get(i).getBoundingBox(), location) )
			{
				moles.get(i).wasTapped();
				return true;
			}
		}
		return true;
	}
	
//	public boolean ccTouchesBegan(MotionEvent event) {
//	//super.ccTouchesBegan(event);
//	CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
//	
//	CGPoint dummy = new CGPoint();
//	dummy.set(moles.get(0).getPosition().x, location.y);
//	
//	for(int i = 0; i < moles.size(); i += 6)
//	{			
//		if(CGRect.containsPoint(moles.get(i).getBoundingBox(),dummy)){
//			int length = i + 6;
//			while (i < length) {
//				if( moles.get(i).isUp && CGRect.containsPoint(moles.get(i).getBoundingBox(), location) )
//				{
//					moles.get(i).wasTapped();
//					Context context = CCDirector.sharedDirector().getActivity();
//					SoundEngine.sharedEngine().playEffect(context, R.raw.splat);
//					HUD h = (HUD)tags.getLayerWithTag(tags.TAG_HUD);
//					h.didScore();
//					return true;
//				}
//				i++;
//			}
//			return true;
//		}
//	}
//	return true;
//}
	
}
