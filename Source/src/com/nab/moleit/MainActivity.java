package com.nab.moleit;

import com.nab.tapthemole.AnimUtils;
import com.nab.tapthemole.Constant;
import com.nab.tapthemole.Prefs;
import com.nab.tapthemole.SoundDude;
import com.nab.tapthemole.levelselector.MenuItemActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	ImageView btn_start, btn_home;
	ToggleButton btn_settings, btn_sound, btn_feedbak;
	FrameLayout frame_feedback, info_srceen;
	LinearLayout setting_panel;
	Context mContext;
	Dialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Constant.hideActionBar(MainActivity.this);
		
		setContentView(R.layout.activity_main);
		
		mContext = this;
		
		Prefs.LoadPrefs(getApplicationContext());
		
		//playing background music
		if(Prefs.isSound)
		{
			if(!SoundDude.loaded)
				SoundDude.load(getApplicationContext());
			SoundDude.musicStart();
		}
		
		btn_feedbak = (ToggleButton) findViewById(R.id.btn_feedback);
		btn_start = (ImageView) findViewById(R.id.btn_start);
		btn_settings = (ToggleButton) findViewById(R.id.btn_settings);
		setting_panel = (LinearLayout) findViewById(R.id.settings_panel);
		frame_feedback = (FrameLayout) findViewById(R.id.frame_feedback);
		info_srceen = (FrameLayout) findViewById(R.id.frame_info);
		btn_settings.bringToFront();
		
		btn_sound = (ToggleButton) findViewById(R.id.btn_sound);
		
		frame_feedback.getBackground().setAlpha(75);
		
		btn_home = (ImageView) findViewById(R.id.btn_home);
		
		frame_feedback.setVisibility(View.INVISIBLE);
		setting_panel.setVisibility(View.INVISIBLE);
		info_srceen.setVisibility(View.INVISIBLE);
		
		frame_feedback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				btn_feedbak.performClick();
				clickable(true);
				frame_feedback.setClickable(false);
			}
		});
	}
	
	public void onClickBtn(View v)
	{
		ImageView btn = (ImageView) v;
		
		switch (btn.getId()) 
		{
		
			case R.id.btn_start:
				SoundDude.btnSound(mContext, R.raw.music_btn_snd);
				Intent menu_intent = new Intent(MainActivity.this, MenuItemActivity.class);
				startActivity(menu_intent);
				closeSettingOption();
				break;
				
			case R.id.btn_info:
				SoundDude.btnSound(mContext, R.raw.music_btn_snd);
				AnimUtils.slideToDown2(info_srceen, mContext);
				info_srceen.setVisibility(View.VISIBLE);
				info_srceen.bringToFront();
				btn_home.setClickable(true);
				clickable(false);
				closeSettingOption();
				break;
				
			case R.id.btn_store:
				SoundDude.btnSound(mContext, R.raw.store_snd);
				Intent storeIntent = new Intent(MainActivity.this, StoreActivity.class);
				startActivity(storeIntent);
				closeSettingOption();
				break;
				
			case R.id.btn_home:
				SoundDude.btnSound(mContext, R.raw.home_btn_snd);
				AnimUtils.slideToUp2(info_srceen, mContext);
				info_srceen.setVisibility(View.GONE);
				btn_home.setClickable(false);
				clickable(true);
				closeSettingOption();
				break;
			case R.id.feedback_image:
				SoundDude.btnSound(mContext, R.raw.music_btn_snd);
				openAppLink();
				btn_feedbak.performClick();
				break;
			default:
				break;
		}		
	}
	
	Handler mHandler = new Handler();
	Runnable mrRunnable = new Runnable() {
		
		@Override
		public void run() {
			
			closeSettingOption();
		}
	};
	
	public void closeSettingOption()
	{
		if(btn_settings.isChecked())
		{
			AnimUtils.slideToDown(setting_panel, mContext);
			mHandler.removeCallbacks(mrRunnable);
			btn_settings.setChecked(false);
		}
	}
	
	public void openAppLink()
	{
		String packageName = this.getPackageName();
		try 
		{		
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+packageName )));
		} 
		catch (android.content.ActivityNotFoundException anfe) 
		{
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packageName)));
		}
	}
	
	public void btnIsCheck(ToggleButton btn)
	{
		if(btn.isChecked())
		{
			btn.performClick();
		}
	}
	
	public void onToggleClick(View v)
	{
		ToggleButton btn =(ToggleButton) v;
		switch (btn.getId()) 
		{
		case R.id.btn_feedback:
			findViewById(R.id.feedback_image).setClickable(btn.isChecked());
			if(btn.isChecked())
			{
				frame_feedback.bringToFront();
				AnimUtils.slideToLeft(frame_feedback, mContext);
				SoundDude.btnSound(getApplicationContext(), R.raw.star_snd);
				closeSettingOption();
				clickable(false);
				frame_feedback.setClickable(true);
			}
			else
			{
				AnimUtils.slideToRight(frame_feedback, mContext);
				frame_feedback.setClickable(false);
				clickable(true);
			}
			break;
			
		case R.id.btn_settings:
			SoundDude.btnSound(getApplicationContext(), R.raw.settings_snd);
			settingsBtnClickable(btn.isChecked());
			if(btn.isChecked())
			{
				
				AnimUtils.slideToUp(setting_panel, mContext);
				mHandler.postDelayed(mrRunnable, 5000);
			}
			else
			{
				AnimUtils.slideToDown(setting_panel, mContext);
				mHandler.removeCallbacks(mrRunnable);
				
			}
			break;
		case R.id.btn_sound:
			SoundDude.btnSound(getApplicationContext(), R.raw.music_btn_snd);
			Prefs.setSound(btn.isChecked());
			if(Prefs.isSound)
			{

				if(!SoundDude.loaded)
					SoundDude.load(getApplicationContext());
				SoundDude.musicStart();
			}
			else
			{
				SoundDude.musicPause();
				SoundDude.loaded=false;	
			}
			break;

		default:
			break;
		}
		
	}
	
	public void settingsBtnClickable(boolean clickable)
	{
		findViewById(R.id.btn_facebook).setClickable(clickable);
		findViewById(R.id.btn_twitter).setClickable(clickable);
		findViewById(R.id.btn_info).setClickable(clickable);
		findViewById(R.id.btn_store).setClickable(clickable);
		findViewById(R.id.btn_sound).setClickable(clickable);
		
	}
	
	public void clickable(boolean clickable)
	{
		btn_settings.setClickable(clickable);
		btn_feedbak.setClickable(clickable);
		btn_start.setClickable(clickable);
	}

	@Override
	public void onResume() {
		super.onResume();
		
		btn_sound.setChecked(Prefs.isSound);
		if(!Prefs.isSound)
		{
			SoundDude.musicPause();
		}
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		SoundDude.musicStop();
	}
	
}