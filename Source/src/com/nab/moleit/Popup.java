package com.nab.moleit;

import java.util.List;

import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGSize;

public class Popup extends CCSprite {
	CCMenu menu = null;

	public Popup(String title) {
		CGSize winSize = CCDirector.sharedDirector().winSize();
		this.setDisplayFrame(CCSpriteFrameCache.spriteFrameByName("menu.png"));
		this.setPosition(winSize.width / 2.0f, winSize.height / 2.0f);
		this.setScaleX(tags.rX);
		this.setScaleY(tags.rY);

		CCLabel label = CCLabel.makeLabel(title, "TOONISH.ttf", 32);
		label.setPosition(this.getContentSize().width / 2.0f,
				this.getContentSize().height - label.getContentSize().height
						* 2);
		this.addChild(label, 0);

		setVisible(false);
	}

	public void show(boolean isShow) {
		Game g = (Game) tags.getLayerWithTag(tags.TAG_GAME_LAYER);
		if (isShow) {
			g.pauseSchedulerAndActions();
		} else{
			g.resumeSchedulerAndActions();
		}
		List<CCNode> children = g.getChildren();
		for(int i = 0; i < children.size(); i++)
		{
			if(isShow)
			{
				children.get(i).pauseSchedulerAndActions();
			}
			else
			{
				children.get(i).resumeSchedulerAndActions();
			}
		}
		setVisible(isShow);
		if(isShow)
		{
			SoundEngine.sharedEngine().pauseSound();
		}
		else
		{
			SoundEngine.sharedEngine().resumeSound();
		}
	}

	public void addButtonWithText(String text, Object target, String handler) {
		CCLabel label = CCLabel.makeLabel(text, "TOONISH.ttf", 24);
		CCSprite buttonSprite = CCSprite.sprite(CCSpriteFrameCache
				.spriteFrameByName("button_small.png"));
		//CCSprite bigButtonSprite = CCSprite.sprite(CCSpriteFrameCache
			//	.spriteFrameByName("button_big.png"));
		CCMenuItemSprite playButton = CCMenuItemSprite.item(buttonSprite,
				buttonSprite, (CCNode) target, handler);
		label.setPosition(playButton.getContentSize().width / 2,
				playButton.getContentSize().height / 2);
		playButton.addChild(label, 1);
		if (menu == null) {
			menu = CCMenu.menu(playButton);
			menu.setPosition(getContentSize().width / 2f,
					getContentSize().height / 2f - getContentSize().height / 8f);
			this.addChild(menu, 1);
		} else {
			menu.addChild(playButton, 0);
		}
		menu.alignItemsHorizontally(playButton.getContentSize().width / 4);
	}

	public static Popup getPopupWithTitle(String title) {
		Popup popup = new Popup(title);
		return popup;
	}
}
