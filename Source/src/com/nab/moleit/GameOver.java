package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;

public class GameOver extends CCLayer {
	public Popup popup;
	public GameOver()
	{
		popup = Popup.getPopupWithTitle("--Game Over--");
		addChild(popup,1);
	}
	
	public void gameOver()
	{
		popup.show(true);
		popup.addButtonWithText("Replay", this, "replay");
		popup.addButtonWithText("Main", this, "mainMenu");
		Pause p = (Pause)tags.getLayerWithTag(tags.TAG_PAUSE);
		p.menu.setVisible(false);
		p.popup.cleanup();
	}
	public void replay(Object sender)
	{
		CCDirector.sharedDirector().replaceScene(Game.scene());
	}
	public void mainMenu(Object sender)
	{
		CCDirector.sharedDirector().getActivity().finish();
	}
}
