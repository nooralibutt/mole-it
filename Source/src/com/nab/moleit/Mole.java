package com.nab.moleit;

import android.annotation.SuppressLint;
import android.content.Context;

import java.util.ArrayList;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.instant.CCCallFuncN;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.sound.SoundEngine;

@SuppressLint("DefaultLocale")
public class Mole extends CCSprite {
	
	public float upTime;
	public boolean isUp, didMiss;
	public int moleType;
	
	public static final int STAR = 0;
	public static final int BLUE_MOLE = 1;
	public static final int GREEN_MOLE = 2;
	public static final int YELLOW_MOLE = 3;
	public static final int BOMB = 4;
	public static final int CLOCK = 5;
	public static final int TOTAL_MOLES = 6;
	
	public static final String [] ANIMATION_NAMES = {"star", "b", "a", "c", "bomb", "clock"};		
	
	public Mole() {
		upTime = 2.0f;
		isUp = false;
		this.setDisplayFrame(CCSpriteFrameCache.spriteFrameByName("a0001.png"));
	}

	boolean getIsUp() {
		return isUp;
	}

	public void start(int moleType) {
		this.stopAllActions();
		
		this.moleType = moleType;
		isUp = true;
		didMiss = false;

		runAction(CCSequence.actions(CCDelayTime.action(upTime),
				CCCallFuncN.action(this, "stop")));
		CCCallFuncN moveStart = CCCallFuncN.action(this, "startLoopAnimation");
		CCAnimate animate = getAnimationWithFrames(1, 10);
		runAction(CCSequence.actions(animate, moveStart));
	}

	public void stop(Object sender) {
		this.stopAllActions();
		CCCallFuncN moveStart = CCCallFuncN.action(this, "reset");
		CCAnimate animate = getAnimationWithFrames(1, 10).reverse();
		runAction(CCSequence.actions(animate, moveStart));

	}

	public void startLoopAnimation(Object sender)
	{
		if(!isUp){
			return;
		}
		CCRepeatForever repeat = CCRepeatForever.action(CCSequence.actions( getAnimationWithFrames(11,20),
				getAnimationWithFrames(11,20).reverse()));
		repeat.setTag( tags.TAG_REPEAT_ANIM);
		this.runAction(repeat);
	}

	CCAnimate getAnimationWithFrames(int from, int to) {
		ArrayList<CCSpriteFrame> frames = new ArrayList<CCSpriteFrame>();
		for (int i = from; i <= to; i++) {
			String str = String.format(ANIMATION_NAMES[moleType] + "%04d.png", i);
			frames.add(CCSpriteFrameCache.spriteFrameByName(str));
		}
		CCAnimate animate = CCAnimate.action(CCAnimation.animation("c0011.png",
				1.0f / 24.0f, frames));
		return animate;
	}
	
	CCAnimate getExplosionAnimationFrames() {
		ArrayList<CCSpriteFrame> frames = new ArrayList<CCSpriteFrame>();
		for (int i = 1; i <= 14; i++) {
			String str = String.format("explosion%04d.png", i);
			frames.add(CCSpriteFrameCache.spriteFrameByName(str));
		}
		frames.add(CCSpriteFrameCache.spriteFrameByName("a0001.png"));
		CCAnimate animate = CCAnimate.action(CCAnimation.animation("a0001.png",
				1.0f / 24.0f, frames));
		return animate;
	}
	
	public void reset(Object sender)
	{
		tags.molesUpCount--;
		isUp = false;
		if(didMiss)
		{
			HUD h = (HUD)tags.getLayerWithTag(tags.TAG_HUD);
			h.missedMole();
			Context context = CCDirector.sharedDirector().getActivity();
			SoundEngine.sharedEngine().playEffect(context, R.raw.moles_miss);
		}
	}
	
	public void wasTapped()
	{
		this.stopAllActions();
		//runAction(CCSequence.actions(getExplosionAnimationFrames(),CCCallFuncN.action(this, "stopExplosion")));
		runAction(getAnimationWithFrames(21, 31));
		tags.molesUpCount--;
		isUp = false;
		
		Context context = CCDirector.sharedDirector().getActivity();
		SoundEngine.sharedEngine().playEffect(context, R.raw.splat);
		HUD h = (HUD)tags.getLayerWithTag(tags.TAG_HUD);
		h.didScore();
	}
	
	public void stopExplosion(Object sender)
	{
		this.setAnchorPoint(0f, 0f);
	}
	
	public void stopEarly()
	{
		tags.molesUpCount = 0;
		didMiss = false;
		isUp = false;
		this.stop(null);
	}
}
