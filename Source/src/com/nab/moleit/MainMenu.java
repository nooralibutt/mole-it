package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;

import android.content.Context;
import android.util.Log;

public class MainMenu extends CCLayer {
	
	Popup popup;
	
	protected MainMenu() {
		// window size
		CGSize WinSize = CCDirector.sharedDirector().winSize();

		Log.d("Window Size", "Width: " + WinSize.width + " Height: " + WinSize.height);
		
		// handling sound - preloading
		Context context = CCDirector.sharedDirector().getActivity();
		SoundEngine.sharedEngine().preloadSound(context, R.raw.moles_bg);
		SoundEngine.sharedEngine().preloadEffect(context, R.raw.splat);
		SoundEngine.sharedEngine().preloadEffect(context, R.raw.moles_miss);

		// registering texture packer or sprite sheet
		//String file = (WinSize.width > 800) ? "moles-hd.plist" : "moles.plist";
		CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFrames("moles.plist");

		// setting background
		//file = (WinSize.width > 800) ? "title-hd.png" : "title.png";
		CCSprite bg = CCSprite.sprite(context.getResources().getString(R.string.title_screen_name));
		bg.setAnchorPoint(0, 0);
		addChild(bg, -1);
		float rX = WinSize.width / bg.getContentSize().width;
		float rY = WinSize.height / bg.getContentSize().height;
		tags.rX = rX;
		tags.rY = rY;
		bg.setScaleX(rX);
		bg.setScaleY(rY);

		// adding play button
		CCLabel label = CCLabel.makeLabel("PLAY!", "TOONISH.ttf", 40);
		label.setColor(ccColor3B.ccWHITE);
		
		CCSprite buttonSprite = CCSprite.sprite(CCSpriteFrameCache
				.spriteFrameByName("button_big.png"));
		CCMenuItemSprite playButton = CCMenuItemSprite.item(buttonSprite,
				buttonSprite, this, "playGame");
		label.setPosition(CGPoint.ccp(playButton.getContentSize().width / 2,
				playButton.getContentSize().height / 2));
		playButton.addChild(label, 2);
		playButton.setScaleX(rX);
		playButton.setScaleY(rY);
		
		// adding credits button
		CCLabel cLabel = CCLabel.makeLabel("Credits!", "TOONISH.ttf", 32);
		label.setColor(ccColor3B.ccWHITE);
		CCSprite cButtonSprite = CCSprite.sprite(CCSpriteFrameCache
				.spriteFrameByName("button_big.png"));
		CCMenuItemSprite cButton = CCMenuItemSprite.item(cButtonSprite,
				cButtonSprite, this, "credits");
		cLabel.setPosition(CGPoint.ccp(cButton.getContentSize().width / 2,
				cButton.getContentSize().height / 2));
		cButton.addChild(cLabel, 2);
		cButton.setScaleX(rX);
		cButton.setScaleY(rY);
		
		CCMenu menu = CCMenu.menu(playButton,cButton);
		menu.setPosition(WinSize.width / 2f, WinSize.height / 2
				- WinSize.height / 3.5f);
		menu.alignItemsHorizontally(playButton.getContentSize().width/2);
		tags.TAG_PLAY_BUTTON = 7;
		addChild(menu, 1, tags.TAG_PLAY_BUTTON);
	}

	public static CCScene scene() {
		CCScene scene = CCScene.node();
		CCLayer layer = new MainMenu();
		scene.addChild(layer);
		return scene;
	}

	public void playGame(Object sender) {
		CCDirector.sharedDirector().replaceScene(Game.scene());
	}

	static void mainMenu() {
		CCDirector.sharedDirector().replaceScene(MainMenu.scene());
	}
	public void credits(Object sender)
	{
		getChild(tags.TAG_PLAY_BUTTON).setVisible(false);
		if(popup == null){
		popup = Popup.getPopupWithTitle("--Credits--");
		/*String desc = "The Game was originally developed by Todd Perkins in C++ using cocos2d!\nI converted it to " +
				"Java and added some new features and improvements.\nMy name is Noor Ali Butt and " +
				"im studying in Punjab University College of Information Technology!\nSpecial thanks to " +
				"Amad Zafar!";*/
		CCLabel descLabel = CCLabel.makeLabel("The Game was originally developed by Todd Perkins in C++!"
				, "TOONISH.ttf", 14);
		descLabel.setPosition(popup.getContentSize().width/2, 
				popup.getContentSize().height/2f + descLabel.getContentSize().height * 2f);
		popup.addChild(descLabel, 2);
		CCLabel descLabel1 = CCLabel.makeLabel("I  converted it to Java and added some new features and improvements!"
				, "TOONISH.ttf", 12);
		descLabel1.setPosition(popup.getContentSize().width/2, 
				popup.getContentSize().height/2f + descLabel.getContentSize().height);
		popup.addChild(descLabel1, 2);
		CCLabel descLabel2 = CCLabel.makeLabel("Developer: Noor Ali Butt"
				, "TOONISH.ttf", 18);
		descLabel2.setPosition(popup.getContentSize().width/2, 
				popup.getContentSize().height/2 - descLabel1.getContentSize().height * 1.1f);
		popup.addChild(descLabel2, 2);
		CCLabel descLabel4 = CCLabel.makeLabel("Email: nooralibutt@gmail.com"
				, "TOONISH.ttf", 12);
		descLabel4.setPosition(popup.getContentSize().width/2, 
				popup.getContentSize().height/2 - descLabel1.getContentSize().height * 2.5f);
		popup.addChild(descLabel4, 2);
		CCLabel descLabel3 = CCLabel.makeLabel("Special Thanks to: Amad Zafar"
				, "TOONISH.ttf", 12);
		descLabel3.setPosition(popup.getContentSize().width/2,
				popup.getContentSize().height/2 - descLabel1.getContentSize().height * 3.5f);
		popup.addChild(descLabel3, 2);
		//adding button
		CCLabel bLabel = CCLabel.makeLabel("Back!", "TOONISH.ttf", 24);
		CCSprite buttonSprite = CCSprite.sprite(CCSpriteFrameCache
				.spriteFrameByName("button_small.png"));
		CCMenuItemSprite button = CCMenuItemSprite.item(buttonSprite,
				buttonSprite, this, "back");
		bLabel.setPosition(button.getContentSize().width / 2,
				button.getContentSize().height / 2);
		button.addChild(bLabel, 1);
		tags.TAG_BACK_BUTTON = 6;
		CCMenu menu = CCMenu.menu(button);
		menu.setPosition(popup.getContentSize().width - button.getContentSize().width,
				(popup.getContentSize().height * 0.3f ) - button.getContentSize().height/2);
		popup.addChild(menu, 1, tags.TAG_BACK_BUTTON);
		addChild(popup, 1);
		}
		else
			popup.getChild(tags.TAG_BACK_BUTTON).setVisible(true);
		popup.setVisible(true);
	}
	public void back(Object sender)
	{
		popup.getChild(tags.TAG_BACK_BUTTON).setVisible(false);
		popup.setVisible(false);
		getChild(tags.TAG_PLAY_BUTTON).setVisible(true);
	}
}
