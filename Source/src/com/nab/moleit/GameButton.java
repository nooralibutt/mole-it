package com.nab.moleit;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.types.CGSize;

public class GameButton extends CCSprite {
	public GameButton(String text, Boolean isBig)
	{
		CGSize winSize = CCDirector.sharedDirector().winSize();
		String btnFrame = (isBig) ? "button_big.png" : "button_small.png";
	    this.setDisplayFrame(CCSpriteFrameCache.spriteFrameByName(btnFrame));
	    this.setPosition(winSize.getWidth()/2.0f, winSize.height/2.0f);
	    
	    CCLabel label = CCLabel.makeLabel(text,"TOONISH.ttf", 70);
	    label.setPosition(this.getContentSize().width/2.0f,this.getContentSize().height/2.0f);
	    this.addChild(label, 1);
	    
	    /*CCLabelTTF *labelShadow = CCLabelTTF::create(text, CCString::createWithFormat("%s.ttf",FONT_MAIN)->getCString(), fSize + isBig * fSize);
	    labelShadow->setPosition(ccp(this->getContentSize().width/2 - (2 + isBig * 2),this->getContentSize().height/2));
	    labelShadow->setColor(ccBLACK);
	    labelShadow->setOpacity(150);
	    this->addChild(labelShadow,0);
	    */
	    //this->setScale(Utils::getScale());
	}
	static GameButton buttonWithText(String text, Boolean isBig)
	{
		GameButton button = new GameButton(text, isBig);
		return button;
	}
}
