package com.nab.moleit;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;

public class HUD extends CCLayer {
//	public int score;
//	public int highScore;
//	public CCLabel scoreLabel, highScoreLabel;
//	public int carrotsLeft;
//	CCSprite [] carrots;
	
	public int targetMoles, currentlyTappedMoles;
	float targetTime;
	public CCLabel timeLabel, molesLabel, targetMolesLabel;
	
	public HUD()
	{
		CGSize winSize = CCDirector.sharedDirector().winSize();
		
		targetMoles = currentlyTappedMoles = 0;
		
		targetTime = 00.00f;
		
		molesLabel = CCLabel.makeLabel("Moles: " + currentlyTappedMoles, "TOONISH.ttf", 24);
		molesLabel.setScaleX(tags.rX);
		molesLabel.setScaleY(tags.rY);
		molesLabel.setAnchorPoint(0,1);
		molesLabel.setPosition(CGPoint.ccp(10,winSize.height - 
				(molesLabel.getContentSize().height/2)));
		addChild(molesLabel,1);
		
		timeLabel = CCLabel.makeLabel("Time: 00.00", "TOONISH.ttf", 24);
		timeLabel.setPosition(CGPoint.ccp(winSize.width/2 ,winSize.height - 
				(timeLabel.getContentSize().height/2)));
		timeLabel.setAnchorPoint(0.5f,1);
		timeLabel.setScaleX(tags.rX);
		timeLabel.setScaleY(tags.rY);
		addChild(timeLabel,1);
		
		targetMolesLabel = CCLabel.makeLabel("Target: " + targetMoles, "TOONISH.ttf", 24);
		targetMolesLabel.setScaleX(tags.rX);
		targetMolesLabel.setScaleY(tags.rY);
		targetMolesLabel.setAnchorPoint(1,1);
		targetMolesLabel.setPosition(CGPoint.ccp(winSize.width - 10, 
				winSize.height - (targetMolesLabel.getContentSize().height/2)));
		addChild(targetMolesLabel,1);
		
		
		
//		highScore = tags.getHighScore();
//		carrotsLeft = 3;
//		carrots = new CCSprite[carrotsLeft];
//		for(int i = 0; i < carrotsLeft; i++)
//		{
//			carrots[i] = CCSprite.sprite(CCSpriteFrameCache.spriteFrameByName("life.png"));
//			carrots[i].setScaleX(tags.rX);
//			carrots[i].setScaleY(tags.rY);
//			carrots[i].setPosition(winSize.width - (10 + (i * carrots[i].getContentSize().width * tags.rX)) , winSize.height - 10);
//			carrots[i].setAnchorPoint(1,1);
//			addChild(carrots[i],2);
//		}
//		
//		score = 0;
//		scoreLabel = CCLabel.makeLabel("Score: 0", "TOONISH.ttf", 24);
//		scoreLabel.setPosition(CGPoint.ccp(10,winSize.height - 
//				(scoreLabel.getContentSize().height/2)*tags.rY));
//		scoreLabel.setAnchorPoint(0,1);
//		scoreLabel.setScaleX(tags.rX);
//		scoreLabel.setScaleY(tags.rY);
//		addChild(scoreLabel,2);
//		
//		highScoreLabel = CCLabel.makeLabel("High: "+highScore, "TOONISH.ttf", 24);
//		highScoreLabel.setScaleX(tags.rX);
//		highScoreLabel.setScaleY(tags.rY);
//		highScoreLabel.setAnchorPoint(0,1);
//		highScoreLabel.setPosition(CGPoint.ccp(winSize.width/2 - 10,winSize.height - 
//				(highScoreLabel.getContentSize().height/2)*tags.rY));
//		addChild(highScoreLabel,2);
	}
	
	public void update_time(float dt)
	{
		targetTime -= dt;
		
		if(targetTime <= 0){
			GameOver go = (GameOver) tags.getLayerWithTag(tags.TAG_GAMEOVER);
			go.gameOver();
			Game g = (Game) tags.getLayerWithTag(tags.TAG_GAME_LAYER);
			g.setIsTouchEnabled(false);
		}
		else
			timeLabel.setString("Time: " + String.format("%2.2f", targetTime));
			
	}
	
	public void setTargetMoles(int _target){
		targetMoles = _target;
		targetMolesLabel.setString("Moles: " + targetMoles);
	}
	
	public void setTargetTime(float _target){
		targetTime = _target;
		timeLabel.setString("Time: " + String.format("%2.2f", targetTime));
	}
	
	public void didScore()
	{
		currentlyTappedMoles += 1;
		molesLabel.setString("Moles: " + currentlyTappedMoles);
		
		if(currentlyTappedMoles >= targetMoles){
			GameOver go = (GameOver) tags.getLayerWithTag(tags.TAG_GAMEOVER);
			go.gameOver();
			Game g = (Game) tags.getLayerWithTag(tags.TAG_GAME_LAYER);
			g.setIsTouchEnabled(false);
		}
//		score += 1;
//		scoreLabel.setString("Score: "+score);
//		if(score > highScore)
//		{
//			highScore = score;
//			highScoreLabel.setString("High: "+highScore);
//		}
	}
	public void missedMole()
	{
//		carrotsLeft--;
//		carrots[carrotsLeft].setVisible(false);
//		if(carrotsLeft == 0)
//		{
//			GameOver go = (GameOver)tags.getLayerWithTag(tags.TAG_GAMEOVER);
//			go.gameOver();
//			Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
//			g.setIsTouchEnabled(false);
//			if(score == highScore)
//				tags.saveHighScore(highScore);
//		}
//		if(carrotsLeft > 0){
//			Game g = (Game)tags.getLayerWithTag(tags.TAG_GAME_LAYER);
//			ArrayList<Mole> moles = g.moles;
//			for (int i = 0; i < moles.size(); i++) {
//				if(moles.get(i).isUp){
//					moles.get(i).stopEarly();
//				}
//			}
//		}
	}
	@Override
	public void onExit() {
		super.onExit();
//		carrots = null;
	}	
}
