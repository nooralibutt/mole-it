package com.nab.tapthemole.levelselector;


import com.nab.moleit.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class MenuGridAdapter extends BaseAdapter 
{

	Context mContext;
	int[] items;
	int pd;
	
	public MenuGridAdapter(Context mContex, int[] res_id,int padding) {
		this.mContext = mContex;
		this.items = res_id;
		this.pd=padding;
	}

	@Override
	public int getCount() {
		return items.length;
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup container) {
		
		View view = convertView;
		ImageView imageView = null;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater
					.inflate(R.layout.custom_grid_item, container, false);

		}
		imageView = (ImageView) view.findViewById(R.id.grid_item_image);
		// imageView.setLayoutParams(new GridView.LayoutParams(160,160));
		imageView.setImageResource(items[position]);
		imageView.setPadding(pd, pd, 0, pd);
		return view;
		
	}

}
