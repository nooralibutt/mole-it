package com.nab.tapthemole.levelselector;

import java.util.ArrayList;
import java.util.Arrays;

import com.nab.moleit.Application;
import com.nab.moleit.R;
import com.nab.tapthemole.levelselector.JazzyViewPager;
import com.nab.tapthemole.levelselector.MenuGridAdapter;
import com.nab.tapthemole.levelselector.OutlineContainer;
import com.nab.tapthemole.levelselector.JazzyViewPager.TransitionEffect;
import com.nab.tapthemole.Constant;
import com.nab.tapthemole.Prefs;
import com.nab.tapthemole.SoundDude;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import android.os.Bundle;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;


public class MenuItemActivity extends Activity {
	
	public static final String EXTRA_LEVEL_NO = "LEVEL_NO";
	public static final int LEVELS_AT_SINGLE_GRID = 6;
	
	private Animation zoom = null;
    private View lastView = null;
	Context mContex;
	int lastpage = 0;
	
	//total grids
	private static int NUM_PAGES = 2;
	
	private JazzyViewPager mPager;
	ArrayList<MenuItems> arr_grid_list = new ArrayList<MenuItems>();
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.menu_item_activity);
		mContex = this;
		 
		int[] item_list = Get_itemList();
		
		int i;
		for(i = 0; i + LEVELS_AT_SINGLE_GRID < item_list.length; i += LEVELS_AT_SINGLE_GRID){
			int[] subList = Arrays.copyOfRange(item_list, i, i + LEVELS_AT_SINGLE_GRID);
			if(subList != null)
				arr_grid_list.add(new MenuItems(0, subList));
		}
		
		int remainder = item_list.length % LEVELS_AT_SINGLE_GRID;
		if(remainder != 0){
			int[] subList = Arrays.copyOfRange(item_list, i, i + remainder);
			if(subList != null)
				arr_grid_list.add(new MenuItems(0, subList));
		}
		
		NUM_PAGES = arr_grid_list.size();
		
		setupJazziness(TransitionEffect.Standard);
	}
	
	private void setupJazziness(TransitionEffect effect) 
	{
		
		mPager = (JazzyViewPager) findViewById(R.id.menu_item_viewpager);
		mPager.setTransitionEffect(effect);
		mPager.setAdapter(new GridItemAdapter());
		mPager.setPageMargin(0);
		ButtonsHide(0);
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				ButtonsHide(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	
	private class GridItemAdapter extends PagerAdapter {
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			
			FrameLayout frame = new FrameLayout(MenuItemActivity.this);
			
			//GridView mGridView = new GridView(mContex);
			LayoutInflater inflater = (LayoutInflater) mContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.menu_item_grid_layout, null);
			
			Drawable d = getResources().getDrawable(getResources().getIdentifier("thumbs_a", "drawable", mContex.getPackageName()));
			int col_width = d.getIntrinsicHeight();
			
			if(Constant.DIFF_SCREEN_SIZE)
			{
				col_width = Constant.SIZE.GRIDVIEW_ITEM_SIZE;
			}
			
			int spacing = (int) (col_width*.089);
			int gw = (int) (col_width*3.15+(spacing*2));
			
//			int pd_left = (int) (spacing*1.1);
			int pd_top = (int) (spacing);
			
			LinearLayout layout_gridview = (LinearLayout) view.findViewById(R.id.layout_gridview);
			
			GridView mGridView = (GridView) view.findViewById(R.id.gridView);
			
			mGridView.getLayoutParams().height=LinearLayout.LayoutParams.MATCH_PARENT;
			mGridView.setHorizontalSpacing((int) (spacing*.5));
			layout_gridview.setPadding(0, pd_top, 0, pd_top);
			mGridView.setPadding((int) (spacing), 0, 0, 0);
			
			mGridView.getLayoutParams().width=gw;
			LinearLayout.LayoutParams lp = (android.widget.LinearLayout.LayoutParams) mGridView.getLayoutParams();
			lp.setMargins(0, 0, 0, 0);
			mGridView.setNumColumns(3);
			mGridView.setStretchMode(GridView.NO_STRETCH);
			mGridView.setColumnWidth(col_width);
			mGridView.setGravity(Gravity.CENTER);
			MenuGridAdapter adapter = new MenuGridAdapter(mContex, arr_grid_list.get(position).getRes_id(), pd_top);
			mGridView.setAdapter(adapter);
			mGridView.setSelector(android.R.color.transparent);
			mGridView.setOnItemClickListener(new OnItemClickListener() 
			{

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					
					int levelNo = (mPager.getCurrentItem() * LEVELS_AT_SINGLE_GRID) + position + 1;
					zoom = AnimationUtils.loadAnimation(mContex, R.anim.scale);
					final Intent coloringIntent = new Intent(MenuItemActivity.this, Application.class);
					coloringIntent.putExtra(EXTRA_LEVEL_NO , levelNo);
					
					//checking
					Toast.makeText(MenuItemActivity.this, "position clicked: " + levelNo, Toast.LENGTH_LONG).show();
					
					if(Prefs.isSound)
					{
						SoundDude.btnSound(mContex, R.raw.left_arrow_snd,coloringIntent);
					}
					else
					{
						zoom.setAnimationListener(new AnimationListener() {
							
							@Override
							public void onAnimationStart(Animation animation) {
							}
							
							@Override
							public void onAnimationRepeat(Animation animation) {
							}
							
							@Override
							public void onAnimationEnd(Animation animation) {
								startActivity(coloringIntent);
							}
						});
					}
					
					try 
					{ 
						if (null != lastView) 
							lastView.clearAnimation();
		            } 
					catch (Exception clear) 
					{ }

		            // Zoom the new selected view
		            try { view.startAnimation(zoom); } catch (Exception animate) {}
		            
		            // Set the last view so we can clear the animation 
		            lastView = view;
					
				}
				
			});
			
			//mGridView.setBackgroundColor(getResources().getColor(R.color.blue));
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.gravity= Gravity.CENTER;
			frame.addView(view,params);
			container.addView(frame, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
			mPager.setObjectForPosition(frame, position);
			return frame;
		}
		@Override
		public void destroyItem(ViewGroup container, int position, Object obj) {
			container.removeView(mPager.findViewFromObject(position));
		}
		@Override
		public int getCount() {
			return NUM_PAGES;
		}
		@Override
		public boolean isViewFromObject(View view, Object obj) {
			if (view instanceof OutlineContainer) {
				return ((OutlineContainer) view).getChildAt(0) == obj;
			} else {
				return view == obj;
			}
		}		
	}
	
	public int[] Get_itemList()
	{
		String[] levels = getResources().getStringArray(R.array.levels);
		int[] arr_items=new int[levels.length];
		
		for(int i = 0; i < levels.length; i++)
		{
			arr_items[i] = mContex.getResources().
					getIdentifier(levels[i], "drawable", mContex.getPackageName());
		}
		return arr_items;
	}
	
	public void onClickBtn(View v)
	{
		switch (v.getId()) {
		case R.id.btn_home_menu_item:
				SoundDude.btnSound(getApplicationContext(), R.raw.home_btn_snd);
				finish();
				break;
		case R.id.btn_back_menu_item:
				SoundDude.btnSound(getApplicationContext(), R.raw.left_arrow_snd);
				btnPressScrolling(-1);
				break;
		case R.id.btn_next_menu_item:
				SoundDude.btnSound(getApplicationContext(), R.raw.right_arrow_snd);
				btnPressScrolling(1);
				break;
		default:
			break;
		}
	}
	
	public void btnPressScrolling(int plusMinus)
	{
		int changePage = mPager.getCurrentItem() + plusMinus;
		if(changePage>=0 && changePage<=NUM_PAGES-1)
		{
			mPager.setCurrentItem(changePage);
		}
	}
	
	public void ButtonsHide(int position)
	{
		if(position != lastpage)
			SoundDude.btnSound(mContex, R.raw.left_arrow_snd);
		if(position==0)
		{
			findViewById(R.id.btn_back_menu_item).setVisibility(View.GONE);
			findViewById(R.id.btn_next_menu_item).setVisibility(View.VISIBLE);
		}
		else if(position==NUM_PAGES-1)
		{
			findViewById(R.id.btn_next_menu_item).setVisibility(View.GONE);
			findViewById(R.id.btn_back_menu_item).setVisibility(View.VISIBLE);
		}
		else
		{
			findViewById(R.id.btn_next_menu_item).setVisibility(View.VISIBLE);
			findViewById(R.id.btn_back_menu_item).setVisibility(View.VISIBLE);
		}
		
		lastpage=position;
	}
	
	public class MenuItems
	{
		public int id;
		 public int res_id[];
		 
		 public MenuItems(int id, int res_id[]) {
		  this.id = id;
		  this.res_id = res_id;
		 }
		 
		 public int getId() {
			return id;
		}
		 
		 public int[] getRes_id() {
			return res_id;
		}
		 
		 public void setId(int id) {
			this.id = id;
		}
		 public void setRes_id(int[] res_id) {
			this.res_id = res_id;
		}
	}
	
	@Override
	public void onResume() {
		if (null != lastView) 
			lastView.clearAnimation();
		super.onResume();
		
	}
}
