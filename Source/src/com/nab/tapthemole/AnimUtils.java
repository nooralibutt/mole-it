package com.nab.tapthemole;

import com.nab.moleit.R;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

public class AnimUtils {
  public static void setLayoutAnim_slidedownfromtop(
      ViewGroup panel, Context ctx) {
    AnimationSet set = new AnimationSet(true);
    Animation animation = new AlphaAnimation(0.0f, 1.0f);
    animation.setDuration(100);
    set.addAnimation(animation);
    animation = new TranslateAnimation(
        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
        Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
    );
    animation.setDuration(500);
    set.addAnimation(animation);

    LayoutAnimationController controller =
        new LayoutAnimationController(set, 0.25f);
    panel.setLayoutAnimation(controller);
  }
  
  public static void setLayoutAnim_slideupfrombottom(
      ViewGroup panel, Context ctx) {
    AnimationSet set = new AnimationSet(true);
    Animation animation = new AlphaAnimation(0.0f, 1.0f);
    animation.setDuration(100);
    set.addAnimation(animation);
    animation = new TranslateAnimation(
        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
        Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f
    );
    animation.setDuration(500);
    set.addAnimation(animation);

    LayoutAnimationController controller =
        new LayoutAnimationController(set, 0.25f);
    panel.setLayoutAnimation(controller);
  }
  
  public static void setLayoutAnim_slideupfrombottom2(
	      ViewGroup panel, Context ctx) {
	    AnimationSet set = new AnimationSet(true);
	    Animation animation = new AlphaAnimation(0.0f, 1.0f);
	    animation.setDuration(100);
	    set.addAnimation(animation);
	    animation = new TranslateAnimation(
	        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	        Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f
	    );
	    animation.setDuration(500);
	    set.addAnimation(animation);
	    
	    LayoutAnimationController controller =
	        new LayoutAnimationController(set, 0.25f);
	    panel.setLayoutAnimation(controller);

  }
  
  public static void setLayoutAnim_slidedownfromtop2(
	      ViewGroup panel, Context ctx) {
	    AnimationSet set = new AnimationSet(true);
	    Animation animation = new AlphaAnimation(0.0f, 1.0f);
	    animation.setDuration(100);
	    set.addAnimation(animation);
	    animation = new TranslateAnimation(
	        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	        Animation.RELATIVE_TO_SELF, -1*panel.getHeight(), Animation.RELATIVE_TO_SELF, 0.0f
	    );
	    animation.setDuration(500);
	    set.addAnimation(animation);

	    LayoutAnimationController controller =
	        new LayoutAnimationController(set, 0.25f);
	    panel.setLayoutAnimation(controller);
	  }
  
  public static void slideToUp(LinearLayout panel,Context ctx)
  {
		TranslateAnimation animate = new TranslateAnimation(0,0,(float) (panel.getHeight()*1.5),0);
	 	animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
  }
  
  public static void slideToDown(LinearLayout panel,Context ctx)
  {
		
		TranslateAnimation animate = new TranslateAnimation(0,0,0,(float) (panel.getHeight()*1.5));
		animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
	
  }
  
  public static void slideToRight(View panel,Context ctx)
  {
		TranslateAnimation animate = new TranslateAnimation(0,-panel.getWidth(),0,0);
	 	animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
  }
  
  
  public static void slideToLeft(View panel,Context ctx)
  {
		
		TranslateAnimation animate = new TranslateAnimation(-panel.getWidth(),0,0,0);
		animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
	
  }
  public static void slideToLeft_ColorView(View panel,Context ctx)
  {
		
		TranslateAnimation animate = new TranslateAnimation((float) (panel.getWidth()*2.2),0,0,0);
		animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
	
  }
  
  public static void slideToRight_ColorView(View panel,Context ctx)
  {
		TranslateAnimation animate = new TranslateAnimation(0,(float) (panel.getWidth()*2.2),0,0);
	 	animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
  }
  
  
  public static void slideToUp2(ViewGroup panel,Context ctx)
  {
		TranslateAnimation animate = new TranslateAnimation(0,0,0,-panel.getHeight());
	 	animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
  }
  
  public static void slideToDown2(ViewGroup panel,Context ctx)
  {
		
		TranslateAnimation animate = new TranslateAnimation(0,0,-panel.getHeight(),0);
		animate.setDuration(500);
		animate.setFillAfter(true);
		panel.startAnimation(animate);
	
  }
  
  public static  void FadeOutAnimationOn(View panel, Context ctx) {
	  Animation animation = AnimationUtils.loadAnimation(ctx,R.anim.enter);
	  panel.startAnimation(animation);
	  
	}
  
  public static  void FadeOutAnimationOff(View panel, Context ctx) {
	  Animation animation = AnimationUtils.loadAnimation(ctx,R.anim.exit);
	  panel.startAnimation(animation);
	  
	}
}
