package com.nab.tapthemole;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;
import android.util.Log;
import java.io.IOException;
import java.util.Random;

import com.nab.moleit.R;

public class SoundDude
{
  private static float effectsVolume = 0.0F;
  public static boolean loaded = false;
  private static int menu;
  private static MediaPlayer mp,mpBtn;
  private static float musicVolume = 0.0F;
  public static boolean muted;
  private static int[] next;
  private static int qty_loaded;
  private static Random rand;
  private static SoundPool sp;
  private static float tmpEffectsVolume = 0.0F;
  private static float tmpMusicVolume = 0.0F;

  static
  {
    qty_loaded = 0;
  }

  public static void load(Context paramContext)
  {
    rand = new Random(System.nanoTime());
    sp = new SoundPool(5, 3, 0);
    mp = MediaPlayer.create(paramContext, R.raw.bg_music);
    sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener()
    {
      @Override
	public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        Log.d("SOUND", "sound: " + paramAnonymousInt1);
        if (SoundDude.qty_loaded == 4)
        {
          SoundDude.mp.setLooping(true);
          SoundDude.mp.setVolume(SoundDude.musicVolume, SoundDude.musicVolume);
          SoundDude.mp.start();
          SoundDude.loaded = true;
        }
      }
    });
    try
    {
      AssetManager localAssetManager = paramContext.getAssets();
      menu = sp.load(localAssetManager.openFd("menu2.ogg"), 1);
      next = new int[3];
      next[0] = sp.load(localAssetManager.openFd("next1.ogg"), 1);
      next[1] = sp.load(localAssetManager.openFd("next2.ogg"), 1);
      next[2] = sp.load(localAssetManager.openFd("next3.ogg"), 1);
      return;
    }
    catch (IOException localIOException)
    {
      Log.d("SOUND", "Error. Cant load sounds");
    }
  }

  public static void menuSound()
  {
    if (loaded)
      sp.play(menu, effectsVolume, effectsVolume, 1, 0, 1.0F);
  }

  public static void musicPause()
  {
    try
    {
      if (mp.isPlaying())
        mp.pause();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
  }

  public static void musicStart()
  {
    try
    {
      if (!mp.isPlaying())
      {
    	  mp.start();
    	  mp.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer arg0) {
				mp.start();
			}
		});
      }
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
  }

  public static void btnSound(Context ctx,int rawID) {
      if(!Prefs.isSound)
    	  return;
	  if (mpBtn != null) {
          //mpBtn.reset();
          mpBtn.release();
          mpBtn=null;
      }
      mpBtn = MediaPlayer.create(ctx, rawID);
      mpBtn.start();
      mpBtn.setOnCompletionListener(new OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer arg0) {
			mpBtn.release();
		}
	});
  }
  
  public static void btnSound(final Context ctx,int rawID,final Intent i) {
      if(!Prefs.isSound)
      {
    	  if(i!=null)
    		  ctx.startActivity(i);
    	  return;
      }  
      
	  if (mpBtn != null) {
          //mpBtn.reset();
          mpBtn.release();
          mpBtn=null;
      }
      mpBtn = MediaPlayer.create(ctx, rawID);
      mpBtn.start();
      mpBtn.setOnCompletionListener(new OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer arg0) {
			mpBtn.release();
			if(i!=null)
			{
				ctx.startActivity(i);
			}
		}
	});
  }
  
  public static void musicStop()
  {
    try
    {
      mp.stop();
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
  }

  public static void mute()
  {
    if (sp != null)
    {
      if (!muted)
      {
        muted = true;
        tmpMusicVolume = musicVolume;
        musicVolume = 0.0F;
        tmpEffectsVolume = effectsVolume;
        effectsVolume = 0.0F;
        mp.setVolume(musicVolume, musicVolume);
      }
    }
    else
      return;
    muted = false;
    musicVolume = tmpMusicVolume;
    effectsVolume = tmpEffectsVolume;
    mp.setVolume(musicVolume, musicVolume);
  }

  public static void nextSound()
  {
    if (loaded)
      sp.play(next[rand.nextInt(next.length)], effectsVolume, effectsVolume, 1, 0, 1.0F);
  }

  public static void release()
  {
    if (sp != null)
    {
      sp.release();
      mp.stop();
      mp.release();
    }
  }

  public static void testSound()
  {
    if (loaded)
      sp.play(menu, effectsVolume, effectsVolume, 1, 0, 1.0F);
  }

  /*public static void updateVolume()
  {
    effectsVolume = 0.1F * Prefs.effects;
    musicVolume = 0.1F * Prefs.music;
    if (effectsVolume == 1.0F)
      effectsVolume = 0.99F;
    if (musicVolume == 1.0F)
      musicVolume = 0.99F;
    mp.setVolume(musicVolume, musicVolume);
    Log.d("SOUND", "new effectsVolume: " + effectsVolume + " musicVolume: " + musicVolume);
  }*/
}