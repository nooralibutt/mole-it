package com.nab.tapthemole;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.Window;
import android.view.WindowManager;

public class  Constant 
{
	public static String SHARE_TITLE="Facebook";
	public static Bitmap SHARE_BITMAP = null;
	
	public static boolean DIFF_SCREEN_SIZE=false;
	
	public static int SCREEN_H,SCREEN_W;
	
	public static final String FACEBOOK_APP_ID = "211619498871712";

	public static final String TWITTER_CONSUMER_KEY = "8soLHVfwGLUDn43caYkNEg";
	public static final String TWITTER_CONSUMER_SECRET = "VuoSyQ35b6RYDGadSl8elwyN3bflkwcfWezaqCHSw";

	
	public static String FACEBOOK_SHARE_MESSAGE = "Great ABC learning through Akari Tutoring.";
	public static final String FACEBOOK_SHARE_LINK = "";
	public static final String FACEBOOK_SHARE_ACTION_NAME = "Color me: ABC";
	public static final String FACEBOOK_SHARE_IMAGE_CAPTION = "";

	public static final class Extra {
		public static final String POST_MESSAGE = "Great ABC learning through Akari Tutoring.";
		public static final String POST_LINK = "";
		public static final String POST_PHOTO = "POST_PHOTO";
	}
	
	public static final class SIZE
	{
		public static final int BUTTON_BIG = (int) (SCREEN_H/7.0);
		public static final int BUTTON_MIDIUM =(int) (SCREEN_H/9.0);
		public static final int BUTTON_SMALL =(int) (SCREEN_H/15.0);
		
		public static final int SETTING_PANEL_BOTTOM_MARGIN = (int) (BUTTON_MIDIUM * 1.10);
		
		public static final int BUTTON_START_H =(int) (SCREEN_H/8.3);
		public static final int BUTTON_START_W =(int) (SCREEN_W/4.5);
		
		public static final int IMAGE_FEEDBACK_H = (int) (SCREEN_H/1.88);
		public static final int IMAGE_FEEDBACK_W =(int) (SCREEN_W/1.90);
		
		public static final int MENU_ABC_ITEM_H = (int) (SCREEN_H/2.06);
		public static final int MENU_ABC_ITEM_W = (int) (SCREEN_W/2.16);
		
		public static final int GRIDVIEW_ITEM_SIZE = (int) (SCREEN_H/2.85);
		
		public static final int SHARE_BG_H=(int) (SCREEN_H/2.51);
		public static final int SHARE_BG_W=(int) (SCREEN_W/5.5);
		
		public static final int SHARE_BG_ITEM_H=(int) (SCREEN_H/19.50);
		public static final int SHARE_BG_ITEM_W=(int) (SCREEN_W/7.1);
		
		public static final int SHARE_BRUSH_SIZE = (int) (SCREEN_H/5.5);
		public static final int SHARE_ERASER_SIZE = (int) (SCREEN_H/6);
		
		public static final int SHARE_YES_NO_H=(int) (SCREEN_H/14.50);
		public static final int SHARE_YES_NO_W=(int) (SCREEN_W/8.72);
		
		public static final int SHARE_BACK_H=(int) (SCREEN_H/14.5);
		public static final int SHARE_BACK_W=(int) (SCREEN_H/7.5);
		
		public static final int SHARE_CLEAR_BTN_SIZE=(int) (SCREEN_H/9.7);
		
		public static final int SHARE_EARASER_BRUSH_SIZE_H=(int) (SCREEN_H/7.3);
		public static final int SHARE_EARASER_BRUSH_SIZE_W=(int) (SCREEN_H/4.8);
		
		public static final int SHARE_CLEAR_PAGE_H=(int) (SCREEN_H/4.55);
		public static final int SHARE_CLEAR_PAGE_W=(int) (SCREEN_H/5.4);
		
		public static final int PENCIL_H=(int) (SCREEN_H/3.53);
		public static final int PENCIL_W=(int) (SCREEN_H/10.53);
		
		public static final int COLOR_LISTVIEW_LAYOUT_H = (int) (PENCIL_H/2.1);
		public static final int HOME_BUTTON_MARGIN = 10;
		
	}
	
	public static void setViewSize(Activity act, int rid, int h, int w)
	{
		act.findViewById(rid).getLayoutParams().height=h;
		act.findViewById(rid).getLayoutParams().width=w;
		
	}
	
	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		 
		int width = bm.getWidth();
		 
		int height = bm.getHeight();
		 
		float scaleWidth = ((float) newWidth) / width;
		 
		float scaleHeight = ((float) newHeight) / height;
		 
		// create a matrix for the manipulation
		 
		Matrix matrix = new Matrix();
		 
		// resize the bit map
		 
		matrix.postScale(scaleWidth, scaleHeight);
		 
		
		// recreate the new Bitmap
		 
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
		 
		return resizedBitmap;
		 
		}
	
	public static boolean checkDeviceSize(int h,int w)
	{
		int[] height = new int[]{480,720,800};
		int[] width = new int[]{800,1280,1280};
		
		for(int i=0; i<height.length; i++)
		{
			if(height[i]==h && width[i]==w)
			{
				return false;
			}
		}
		return true;
	}

	public static boolean checkImageSize(int height, int width) {
		if(SCREEN_H==height && SCREEN_W==width)
			return false;
		return true;
	}
	
	public static void hideActionBar(Activity act){
		act.requestWindowFeature(Window.FEATURE_NO_TITLE);
		act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
}

