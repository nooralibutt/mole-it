package com.nab.tapthemole;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs
{
  private static final String PREFS_NAME = "TapTheMole";
  private static final String SOUND = "sound";
  
  static SharedPreferences.Editor editor;
  
  static SharedPreferences settings;
  
  public static boolean isSound;
  
  static int mHeight;
  static int mWidth;
  
  public static void LoadPrefs(Context paramContext)
  {
	settings = paramContext.getSharedPreferences(PREFS_NAME, 0);
    editor = settings.edit();
    isSound = settings.getBoolean(SOUND , true);
  }
  
  public static void setSound(boolean paramBoolean)
  {
	  isSound = paramBoolean;
	  editor.putBoolean(SOUND, paramBoolean).commit();
  }
}