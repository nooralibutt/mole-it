package org.cocos2d.opengl;

@SuppressWarnings("serial")
public class OpenGLViewNotAttachedException extends Exception {
    public OpenGLViewNotAttachedException(String reason) {
        super(reason);
    }
}
