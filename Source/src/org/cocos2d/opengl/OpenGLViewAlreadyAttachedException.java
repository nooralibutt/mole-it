package org.cocos2d.opengl;

@SuppressWarnings("serial")
public class OpenGLViewAlreadyAttachedException extends Exception {
    public OpenGLViewAlreadyAttachedException(String reason) {
        super(reason);
    }
}
