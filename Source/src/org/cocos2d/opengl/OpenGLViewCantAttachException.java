package org.cocos2d.opengl;

@SuppressWarnings("serial")
public class OpenGLViewCantAttachException extends Exception {
    public OpenGLViewCantAttachException(String reason) {
        super(reason);
    }
}
